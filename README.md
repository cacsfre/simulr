# simulr

This [`shiny`][shiny_cran] application can be used to visualize and analyse financial data.

## Quick start for development

We can use [`docker`][docker_install] to run the app through a container.

1. Build the `docker` image, which includes all OS and R dependencies:

```bash
make build  # or make build-clean (--no-cache)
```

> <details><summary>Output (make build)</summary>
>
> ```bash
> user@host:~/simulr$ make build
> docker build --target dev -t simulr/dev .
> Sending build context to Docker daemon  12.46MB
> Step 1/22 : FROM rocker/shiny as prod
>  ---> 4375db10fb7b
>  ...
>  ...
>  ---> Using cache
>  ---> ba1af1b95aea
> Successfully built ba1af1b95aea
> Successfully tagged simulr/dev:latest
> ```
> </details>

2. Enter a temporary (--rm) instance of the above image:

```bash
make run  # or make attach to enter a running simulr/dev container
```

> <details><summary>Output (make run)</summary>
>
> :information_source: the `src` and `app` folders are shared as volumes and mounted inside the container at `/src` and `/app`. All changes made to those files (e.g. with your preferred IDE) will propagate immediately between the host and the container.
>
> ```bash
> user@host:~/simulr$ make run
> docker run -it --rm --network host --name simulr-dev \
>         -v /home/user/simulr/src:/src \
>         -v /home/user/simulr/app:/app \
>         -v /home/user/.bashrc:/root/.bashrc:ro \
>         -v /home/user/.profile:/root/.profile:ro \
>         -v /home/user/.vimrc:/root/.vimrc:ro \
>         -w /src \
>         simulr/dev /bin/bash
> root@host:/src# whoami
> root
> root@host:/src# ls
> data  data-raw  DESCRIPTION  inst  LICENSE.md  Makefile  man  NAMESPACE  R  README.md
> root@host:/src# exit
> exit
> user@host:~/simulr$ whoami
> user
> user@host:~/simulr$ ls src/
> data  data-raw  DESCRIPTION  inst  LICENSE.md  Makefile  man  NAMESPACE  R  README.md
> ```
> </details>
>
> :bulb: Running `R -e "devtools::load_all(); run_simulr()"` will launch the app, which will listen at `http://0.0.0.0:3838`.


## Demo

After building and launching, the app will be available through a web browser.

> <details><summary>Build and launch</summary>
>
> ```bash
> user@host:~/simulr$ make build-prod
> docker build --target prod -t simulr/prod .
> Sending build context to Docker daemon  12.46MB
> Step 1/13 : FROM rocker/shiny as prod
>  ---> 4375db10fb7b
> ...
> Step 12/13 : COPY app /app
>  ---> Using cache
>  ---> 5d481aba30a7
> Step 13/13 : RUN rm -rf /srv/shiny-server/* && ln -s /app/app.R /srv/shiny-server/app.R
>  ---> Using cache
>  ---> 077f0af09a29
> Successfully built 077f0af09a29
> Successfully tagged simulr/prod:latest
>
> user@host:~/simulr$ make launch
> docker run -it --rm -p 3838:3838 --name simulr-prod \
>         simulr/prod  /bin/bash -c "R -e 'simulr::run_simulr()'"
>
> R version 4.2.1 (2022-06-23) -- "Funny-Looking Kid"
> Copyright (C) 2022 The R Foundation for Statistical Computing
> Platform: x86_64-pc-linux-gnu (64-bit)
>
> R is free software and comes with ABSOLUTELY NO WARRANTY.
> You are welcome to redistribute it under certain conditions.
> Type 'license()' or 'licence()' for distribution details.
>
>   Natural language support but running in an English locale
>
> R is a collaborative project with many contributors.
> Type 'contributors()' for more information and
> 'citation()' on how to cite R or R packages in publications.
>
> Type 'demo()' for some demos, 'help()' for on-line help, or
> 'help.start()' for an HTML browser interface to help.
> Type 'q()' to quit R.
>
> > simulr::run_simulr()
> Loading required package: shiny
>
> Listening on http://0.0.0.0:3838
> ```
> </details>
>
> :rocket: The app should now be available at `http://0.0.0.0:3838`.
>
> <details><summary>Demo (UI)</summary>
>
> ![](demo/demo.gif)
>
> </details>

## Customize

The app's source code is available through the `src` folder. You can add/remove `shiny` modules through its R folder.

[shiny_cran]: https://cran.r-project.org/web/packages/shiny/index.html
[docker_install]: https://docs.docker.com/engine/install
