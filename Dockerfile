FROM rocker/shiny as prod
MAINTAINER Carlos C. Sepulveda "cachs1@ulaval.ca"

# System dependencies
RUN apt-get update && apt-get install -y libpq-dev libxml2

# R packages
RUN R -e "install.packages(c('devtools','tseries','coda','TTR'))"
RUN R -e "install.packages(c('RPostgres','dplyr','pool','DBI','dbplyr','RSQLite','websocket'))"
RUN R -e "install.packages(c('shinyMobile','waiter','plotly','DT','shinyWidgets','XML', 'wikitaxa'))"

# Local dependencies
COPY caviarma /tmp/caviarma
COPY src /tmp/simulr
RUN R -e "devtools::install('/tmp/caviarma')"
RUN R -e "devtools::install('/tmp/simulr')"
RUN rm -rf /tmp/simulr && rm -rf /tmp/caviarma 

# Creates empty app content folder 
COPY app /app
# Serves app content
RUN rm -rf /srv/shiny-server/* && ln -s /app/app.R /srv/shiny-server/app.R

FROM prod as dev

RUN apt-get install -y tmux curl git

RUN curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
RUN chmod u+x nvim.appimage && ./nvim.appimage --appimage-extract && rm nvim.appimage
RUN echo "Installed neovim version: $(./squashfs-root/AppRun --version)"
RUN ln -s /squashfs-root/AppRun /usr/bin/nvim
RUN apt-get install -y fonts-powerline
RUN mkdir -p ~/.config/nvim && echo "set runtimepath^=~/.vim runtimepath+=~/.vim/after\nlet &packpath = &runtimepath\nsource ~/.vimrc" >> ~/.config/nvim/init.vim	
RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
