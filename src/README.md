# simulr

This `R` package can be customized to analyse historical and real-time data.

## Quick start

1. Install the current version of this package:

``` r
devtools::install()
```

2. Launch the `shiny` app:

```bash
simulr::run_simulr()
```

## Customize

You can add/remove `shiny` modules through the R folder.
