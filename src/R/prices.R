#' Gets historical OHLC data for cryptocurrencies and traditional securities
#' @return out a \code{list} of two elements:
#'          ohlc : the data as a \code{data.frame}
#'          pair : the name of the currency pair or financial security
#' @export
get_price_hist <-
  function(ticker, limit = 1000, data_step=86400,
  date_from=Sys.Date() - limit, date_to = Sys.Date(), crypto = TRUE) {

  if(crypto) {
    out <- get_crypto_ohlc(
      ticker = ticker,
      limit = limit,
      data_step = data_step
    )
  } else {
    out <- list(
      ohlc = as.data.frame(
        tseries::get.hist.quote(ticker, start = date_from, end = date_to)
      ),
      pair = ticker
    )

    out$ohlc$Date <- as.POSIXct(rownames(out$ohlc))
    colnames(out$ohlc) <- toupper(colnames(out$ohlc))
  }

  out
}

#' Gets a vector of log returns from a OHLC \code{data.frame}.
#' @return ret_vec a named \code{numeric} vector of length \code{n-1}.
#' @export
get_ret_vec <- function(y_data, use_col = colnames(y_data)) {
  use_col <- match.arg(use_col)
  col_idx <- match(use_col, colnames(y_data))
  ret_vec <- diff(log(as.numeric(y_data[, col_idx])))
  names(ret_vec) <- y_data$DATE[-1]
  ret_vec
}
