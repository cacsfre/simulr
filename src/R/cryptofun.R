library(jsonlite)
library(websocket)

get_crypto_ohlc <- function(ticker, data_step = 60, limit = 1) {

  api_call <- sprintf(
    "https://www.bitstamp.net/api/v2/ohlc/%s/?step=%s&limit=%s",
    tolower(ticker), data_step, limit
  )

  out <- jsonlite::fromJSON(api_call)$data

  cols <- c("CLOSE", "HIGH", "LOW", "OPEN", "DATE", "VOLUME")
  colnames(out$ohlc) <- cols

  out$ohlc$DATE <- as.POSIXct(as.numeric(out$ohlc$DATE), origin = "1970-01-01")

  return(out)
}

get_ws_connection <- function(ticker) {
  ws_msg <- sprintf(
    '{"event": "bts:subscribe","data": {"channel": "live_trades_%s"}}',
    tolower(ticker)
  )

  ws <- websocket::WebSocket$new("wss://ws.bitstamp.net") # Websocket API v2

  ws$onOpen(function(event) {
    ws$send(ws_msg)
    message("Websocket Connection opened\n")
  })

  ws$onClose(function(event) {
    message("WebSocket Client disconnected with code ",
            event$code,
            " and reason ",
            event$reason, "\n")
  })
  ws$onError(function(event) {
    warning("WebSocket connection failed with the following message: ", event$message, "\n")
  })

  return(ws)
}


