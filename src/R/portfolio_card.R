portfolio_cardUI <- function(id) {
  tagList(
    conditionalPanel(
      condition = "input.porttable_toggle == true",
      f7Card(
        id = "porttable_card",
        title = "Portfolio",
        div(class="data-table", DT::DTOutput(NS(id, "porttable")))
      )
    ),
    conditionalPanel(
      condition = "input.ranking_toggle == true",
      f7Card(
        title = "Ranking",
        div(class="data-table", DT::DTOutput(NS(id, "ranking")))
      )
    ),
    conditionalPanel(
      condition = "input.transtable_toggle == true",
      f7Card(
        id = "transtable_card",
        title = "Transactions",
        div(class="data-table", DT::DTOutput(NS(id, "transtable")))
      )
    )
  )
}

portfolio_cardServer <- function(id, values, db_pool, user_session_0) {
  moduleServer(id, function(input, output, session) {
    output$ranking <- DT::renderDT(static_ranking_table(db_pool = db_pool))
    output$porttable <- DT::renderDT(static_port_table())
    output$transtable <- DT::renderDT(static_transactions_table(db_pool = db_pool))

    trans <- reactiveFileReader(1000, session, db_path, GetUserTrans,
      traderid = user_session_0$name, db_pool = db_pool)

    rtport <- reactiveFileReader(1000, session, db_path, GetUserPort,
      traderid = user_session_0$name, db_pool = db_pool)

    ranking <- reactiveFileReader(1000, session, db_path, GetRanking,
      db_pool = db_pool)


    observe({
      UpdateRanking(values$user_session, db_pool = db_pool)
      DT::dataTableProxy("ranking", session) %>%
        DT::replaceData(ranking(), rownames = TRUE)
    })

    observe({
      values$user_session$portvalue <- rtport()$port_value + values$cashpos["CAD"]
      DT::dataTableProxy("porttable", session) %>%
        DT::replaceData(rtport()$port_table, rownames = TRUE)
    })

    observe({
      DT::dataTableProxy("transtable", session) %>%
        DT::replaceData(trans(), rownames = TRUE)
    })
  })
}

