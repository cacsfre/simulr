build:
	docker build --target dev -t simulr/dev .

build-clean:
	docker build --target dev --no-cache -t simulr/dev . 

run:
	docker run -it --rm --network host --name simulr-dev \
		-v $(shell pwd)/src:/src \
		-v $(shell pwd)/app:/app \
		-v ${HOME}/.bashrc:/root/.bashrc:ro \
		-v ${HOME}/.profile:/root/.profile:ro \
		-v ${HOME}/.vimrc:/root/.vimrc:ro \
		-w /src \
		simulr/dev /bin/bash

attach: 
	docker exec -it simulr-dev /bin/bash

build-prod:
	docker build --target prod -t simulr/prod .

build-prod-clean:
	docker build --target prod --no-cache -t simulr/prod .

launch:
	docker run -it --rm -p 3838:3838 --name simulr-prod \
		simulr/prod  /bin/bash -c "R -e 'simulr::run_simulr()'"
